/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                        Copyright (c) 2017                             */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/

#include <zircon/assert.h>
#include "flite.h"

extern cst_voice *register_cmu_us_slt(const char *);
extern cst_voice *cmu_us_slt_cg;

cst_voice* flite_fuchsia_create_voice(cst_audio_stream_callback cbk, void* ctx) {
  cst_voice* ret;
  cst_audio_streaming_info* asi = new_audio_streaming_info();

  if (asi == NULL)
    return NULL;

  ZX_DEBUG_ASSERT(cmu_us_slt_cg == NULL);
  register_cmu_us_slt(NULL);

  ret = cmu_us_slt_cg;
  cmu_us_slt_cg = NULL;

  if (ret != NULL) {
    asi->asc = cbk;
    asi->userdata = ctx;
    feat_set(ret->features, "streaming_info", audio_streaming_info_val(asi));
  } else {
    delete_audio_streaming_info(asi);
  }

  return ret;
}
